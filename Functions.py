from math import cos, sin, radians
import numpy as np
from tabulate import tabulate
#np.set_printoptions(precision=3, suppress=True, linewidth=125)

def dsin(value):
    return sin(radians(value))


def dcos(value):
    return cos(radians(value))



# ###DANE##

INPUT_DATA = np.matrix([[1], [2], [3], [4], [5], [6]])
T = [0, 11, 20, 29, 39, 50]
DELTA_T = [0, 11, 9, 9, 10, 10]
X = [6062445, 6062500, 6062556, 6062600, 6062655, 0]
Y = [4368335, 4368343, 4368351, 4368359, 4368367, 0]
PSI = [3.8, 5.1, 6.3, 5.3, 8.8, 0]
V_PRIM = [5.2, 5.2, 5.2, 5.4, 5.3, 0]
OMEGA = [0.13, 0.12, -0.1, 0.35, -0.12, 0]


def acceleration(v, psi, psi_prev, omega, dt): #Ax, Ay
    a_x = round((v*dcos(psi+omega*dt) - dcos(psi_prev))/dt, 3)
    a_y = round((v*dsin(psi+omega*dt) - dsin(psi_prev))/dt, 3)
    return {'x': a_x, 'y': a_y}

AX = [0]
AY = [0]

for i in range(1, len(INPUT_DATA)):
    if i == 0:
        AX.append(acceleration(v=V_PRIM[i], psi=PSI[i], psi_prev=3, omega=OMEGA[i], dt=1)['x'])
        AY.append(acceleration(v=V_PRIM[i], psi=PSI[i], psi_prev=3, omega=OMEGA[i], dt=1)['y'])
    else:
        AX.append(acceleration(v=V_PRIM[i], psi=PSI[i], psi_prev=PSI[i-1], omega=OMEGA[i], dt=DELTA_T[i])['x'])
        AY.append(acceleration(v=V_PRIM[i], psi=PSI[i], psi_prev=PSI[i-1], omega=OMEGA[i], dt=DELTA_T[i])['y'])

print(AX)
print(AY)

NAMES = ['Obs. num', 'Time', 'Delta Time', 'X[m]', 'Y[m]', 'PSI', 'V', 'OMEGA', 'AX', 'AY']
INPUT_DATA = np.c_[INPUT_DATA, T, DELTA_T, X, Y, PSI, V_PRIM, OMEGA, AX, AY]

#AA = np.squeeze(np.asarray(A))
m = np.r_[[NAMES], np.squeeze(np.asarray(INPUT_DATA))]
tab = tabulate(m, tablefmt="fancy_grid")
print (tab)


def transition_matrix_a(delta_t=10):
    trans_matrix = np.matrix([[1, 0, delta_t, 0],
                              [0, 1, 0, delta_t],
                              [0, 0, 1, 0],
                              [0, 0, 0, 1]])
    return trans_matrix





def coordinates_gain(ax, ay, delta_t): #Przyrost współrzędnych w kierunku X,Y #dX, dY
    delta_x = (ax*delta_t**2)/2
    delta_y = (ay*delta_t**2)/2
    return {'x': delta_x, 'y': delta_y}


def coordinates_estimates(x_prev, y_prev, x_gain, y_gain): #Współrzędne estymowane Xk, Yk
    x = x_prev + x_gain
    y = y_prev + y_gain
    return {'x': x, 'y': y}


def state_vector(x, y, v: float, psi: float): #Wektor stanu
    v_cos = v*dcos(psi)
    v_sin = v*dsin(psi)
    return np.matrix([[x], [y], [v_cos], [v_sin]])


def predicted_state_vector(ax: float, ay: float, dt): #Wektor predykcji stanu -> B*uk dla dwuwymiarowego modelu
    element_1 = ax*(dt**2)/2
    element_2 = ay*(dt**2)/2
    element_3 = ax*dt
    element_4 = ay*dt
    return np.matrix([[element_1], [element_2], [element_3], [element_4]])


def noise_vector(sigx, sigy, sigvx, sigvy): #szum procesu WK-1
    return np.matrix([[sigx], [sigy], [sigvx], [sigvy]])


def prediction_state_vector(a, xk_prev, buk, wk_prev):#Wektor predykcji stanu Xk
    return a*xk_prev+buk+wk_prev




def matrix_covariance_state(dt, v, psi, psi_prev):
    v_std = np.std(V_PRIM)
    psi_std = np.std(PSI)

    a11 = dt**2 * ((v_std*dcos(psi_prev))**2 + dt**2 * (psi_std*v*dcos(psi))) #TODO strona 24




'''
axy = acceleration(v=5.2, psi=5.1, psi_prev=3.8, omega=0.12, t=10)


xy_gain = coordinates_gain(ax=axy['ax'], ay=axy['ay'], delta_t=11)


xy_estimate = coordinates_estimates(x_prev=6062445, y_prev=4368335, x_gain=xy_gain['x'], y_gain=xy_gain['y'])

print(xy_estimate)
'''
