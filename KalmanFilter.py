import numpy as np
from tabulate import tabulate
from math import cos as cosinus
from math import sin as sinus
from math import radians


def cos(value: float):
    """
    Zmiana wartości wejściowej funkcji cosinus na stopnie
    :param value: wartość w stopniach
    :return: cosinus kąta podanego w stopniach
    """
    return cosinus(radians(value))


def sin(value: float):
    """
    Zmiana wartości wejściowej funkcji sinus na stopnie
    :param value: wartość w stopniach
    :return: sinus kąta podanego w stopniach
    """
    return sinus(radians(value))


T = [0, 11, 20, 29, 39, 50]
DT = [0, 11, 9, 9, 10, 10]
X = [6062445, 6062500, 6062556, 6062600, 6062655, 0]
Y = [4368335, 4368343, 4368351, 4368359, 4368367, 0]
PSI = [3.8, 5.1, 6.3, 5.3, 8.8, 0]
V_PRIM = [5.2, 5.2, 5.2, 5.4, 5.3, 0]
OMEGA = [0.13, 0.12, -0.1, 0.35, -0.12, 0, 0]
NAMES = ['Obs. num', 'Time', 'Delta Time', 'X[m]', 'Y[m]', 'PSI', 'V', 'OMEGA', 'AX', 'AY']


class Kalman:
    def __init__(self, t, dt, xx, y, psi, v_prim, omega, names):

        self.INPUT_DATA = np.matrix(1)
        for i in range(1, len(t)):
            self.INPUT_DATA = np.insert(self.INPUT_DATA, 0+i, [i+1], 0)
        self.T = t
        self.DT = dt
        self.X = xx
        self.Y = y
        self.PSI = psi
        self.V_PRIM = v_prim
        self.OMEGA = omega
        self.NAMES = names

        self.parameter_check()

    def parameter_check(self):
        """
        Sprawdzenie wielkości / kompatybilności macierzy
        :return: Exception Lenght Error
        """
        if not len(self.T) == len(self.DT) == len(self.X) == len(self.Y) == \
                len(self.PSI) == len(self.V_PRIM) == len(self.OMEGA):
            if not len(self.T) == len(self.DT):
                error_msg = "DT"
            elif not len(self.T) == len(self.X):
                error_msg = "X"
            elif not len(self.T) == len(self.Y):
                error_msg = "Y"
            elif not len(self.T) == len(self.PSI):
                error_msg = "PSI"
            elif not len(self.T) == len(self.V_PRIM):
                error_msg = "V_PRIM"
            elif not len(self.T) == len(self.OMEGA):
                error_msg = "OMEGA"
            else:
                error_msg = "T"
            raise Exception('Param: {} length error'.format(error_msg))

    def table_data(self):
        """
        Tabelaryzacja danych w celu przedstawienia
        :return: numpy array
        """
        table_data = np.c_[self.INPUT_DATA, self.T, self.DT, self.X, self.Y, self.PSI, self.V_PRIM, self.OMEGA]
        # print(table_data)
        names = self.NAMES[0:8]

        table_data = np.r_[[names], np.squeeze(np.asarray(table_data))]
        table_data = tabulate(table_data, tablefmt="fancy_grid")
        return table_data


x = Kalman(t=T, dt=DT, xx=X, y=Y, psi=PSI, v_prim=V_PRIM, omega=OMEGA, names=NAMES)

print(x.table_data())

cos(value=19.8)