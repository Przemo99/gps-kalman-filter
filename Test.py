import numpy as np

from math import cos as cosinus
from math import sin as sinus
from math import radians


def cos(value):
    return cosinus(radians(value))


def sin(value):
    return sinus(radians(value))


print (cos(0))


# #INSERT NA NASTĘPNY ROW
# x = np.matrix(1)
# x = np.insert(x, 1,  [2], 0)
# x = np.insert(x, 2,  [3], 0)
# x = np.insert(x, 3,  [4], 0)
# x = np.insert(x, 4,  [5], 0)
#
#
# print (x)
#
#
# a = 2
# b = 2
# c = 2
#
# if a == b == c:
#     print("tak")

# NAMES = ['Obs. num', 'Time', 'Delta Time', 'X[m]', 'Y[m]', 'PSI', 'V', 'OMEGA', 'AX', 'AY']
#
# print (NAMES[0:5])